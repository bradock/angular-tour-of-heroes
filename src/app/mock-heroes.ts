import { Hero } from './hero';

export const HEROES: Hero[] = [
	{ id: 11, name: 'Lindomar'},
	{ id: 12, name: 'Jerêmias'},
	{ id: 13, name: 'Cleitin'},
	{ id: 14, name: 'Mark'},
	{ id: 15, name: 'Olá'}
];
